var controller = require('../controllers/mainController');

module.exports = function(router, passport) {
  /* GET home page. */
  router.get('/', isLoggedIn, function(req, res, next) {
    res.render('index', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET leads page. */
  router.get('/leadoverview', isLoggedIn, function(req, res) {
    res.render('lead-overview', { title: 'Express', loggedinUser: req.session.name, level: req.session.level});
  });

  /* GET add leads page. */
  router.get('/addnewlead', isLoggedIn, function(req, res) {
    res.render('add-new-lead', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET clients page. */
  router.get('/clientoverview', isLoggedIn, function(req, res) {
    res.render('client-overview', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET clients page. */
  router.get('/addnewclient', isLoggedIn, function(req, res) {
    res.render('add-new-client', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET claims page. */
  router.get('/claimoverview', isLoggedIn, function(req, res) {
    res.render('claim-overview', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET account overview page/admin page. */
  router.get('/accountoverview', isLoggedIn, controller.accountOverview);

  /* GET account overview page/admin page. */
  router.get('/addaccount', isLoggedIn, function(req, res) {
    res.render('add-userprofile', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET account overview page/admin page. */
  router.post('/addaccount', isLoggedIn, controller.newAccountHandler);

  /* GET user profile page. */
  router.get('/userprofile', isLoggedIn, function(req, res) {
    res.render('more-userprofile', { title: 'Express', loggedinUser: req.session.name, level: req.session.level });
  });

  /* GET user profile page. */
  router.get('/moreinvoice', isLoggedIn, function(req, res) {
    res.render('more-invoice', { title: 'Express', loggedinUser: req.session.name, level: req.session.level});
  });

  /* GET user profile page. */
  router.get('/login', function(req, res, next){
    if(req.isAuthenticated()) {
      res.redirect('/');
    } else {
      next();
    }
  }, function(req, res) {
    res.render('more-login', { title: 'Express', loginMessage: req.flash('loginMessage') });
  });

  router.post('/login', function(req, res, next){

      if(req.isAuthenticated()) {
        res.redirect('/');
      } else {
        next();
      }

    }, passport.authenticate('local-login', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  }));

  /* GET logout page. */
  router.get('/logout',function(req, res, next){

      if(!req.isAuthenticated()) {
        notFound404(req, res, next);
      } else {
        next(); 
      }

    }, function(req, res) {
      req.logout();
      res.redirect('/login');
  });

  router.get('/signup', 
    function(req, res, next){

      if(req.isAuthenticated()) {
        res.redirect('/');
      } else {
        next();
      }

    }, function(req, res) {
      res.render('signup', { title: 'Express', signupMessage: req.flash('signupMessage') });
  });

  router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/signupSuccess',
    failureRedirect: '/signup',
    failureFlash: true
  }));

  router.get('/signupSuccess', function(req, res, next){

      if(req.isAuthenticated()) {
        req.logout();
      } else {
        next();
      }

    }, function(req, res) {
      res.render('signup-success', {'title': 'success'});
  });

  router.get('/verify/:token', controller.verifyUser);

  /* get password forgot page */
  router.post('/forgot', controller.passwordReset);
  router.get('/reset/:token', controller.validatePasswordReset);
  router.post('/reset/:token', controller.setPasswordReset);

  /* template render routes */
  router.get('/template/:name', function(req, res) {
    var tpl_name = req.params.name;
    res.render('partials/' + tpl_name);
  });

  // Get the list of users registered in the system
  router.get('/getaccounts', isLoggedIn, controller.listAccounts);

  // get the levels of the currently logged in user
  router.get('/getlevels', isLoggedIn, controller.getUserLevels);

  // get the information of the selected user
  router.get('/user/:id', isLoggedIn, controller.getUserInfo);

  /* middlewares */
  function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
      return next();
    } else {
      res.redirect('/login');
    }
  }

  var notFound404 = function(req, res, next) {
    res.status(404);
    res.render('404', {title: '404 Not Found'});
  };

}