var mysql = require('mysql');
var moment = require('moment');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var validator  = require('validator');
var nodemailer = require('nodemailer');

var db_settings = require('../config/db_config');
var mail_settings = require('../config/mail_config');

var connection = mysql.createConnection(db_settings);
var transporter = nodemailer.createTransport(mail_settings.transport)

/*  Controller that handles user account activation */
function verifyUser(req, res, next) {
    var token = req.params.token;

    connection.query("SELECT * FROM users WHERE verifyToken = '" + token + "'", function(err, rows) {
    	if(err){
	    	res.render('verification-success', {'msg': '500 - Internal Server Error'});
	    }

	    if(!rows.length){
	    	res.render('verification-success', {'msg': 'invalid key - or key expired'});
	    } else {
	    	connection.query("UPDATE users SET verifyToken = 'verified' ", function(err, result) {
	    		if(err){
	    			console.log(err);
	    			res.render('verification-success', {'msg': '500 - Internal Server Error'});
	    		} else {
	    			res.render('verification-success', {'msg': "You're account has been successfully activated - now you can login with your email and password"});
	    		}
	    	});
	    }
    });
};

/* Method that handles the adding of users to the system */
function addNewAccount(req, res, next) {

	// user profile details
	var username = validator.escape(validator.trim(req.body.username)),
		firstname = validator.escape(validator.trim(req.body.firstname)),
		lastname = validator.escape(validator.trim(req.body.lastname)),
		companyRole = validator.escape(validator.trim(req.body.role)),
		email = validator.escape(validator.trim(req.body.email)),
		password = validator.escape(validator.trim(req.body.password));

	// user notification details
	var status_change = req.body.changeAlert || '',
		email_notification = req.body.emailNotification || '',
		send_to = req.body.send2 || '';

	// user security details
	var accessLevels = [
		req.body.acclevel_dashboard || '',
		req.body.acclevel_claims || '',
		req.body.acclevel_billing || '',
		req.body.acclevel_reports || '',
		req.body.acclevel_crm || ''
	].filter(Boolean);

	var branch = req.body.branch || '';

	// validate data
	if(!validator.isEmail(email)) {
		res.end('invalid email');
	}

	// check whether the user already exists
	connection.query("SELECT id FROM users WHERE email = '" + email + "'", function(err, rows) {
		if(err) {
			res.end('database error occured');
		}

		if(rows.length) {
			res.end('Email already taken');
		} else {
			connection.query("SELECT id FROM users WHERE username = '" + username + "'", function(err, rows) {
				if(err) {
					res.end('an error occured');
				}

				if(rows.length) {
					res.end('Username already taken');
				} else {

					bcrypt.genSalt(10, function(err, salt) {
						bcrypt.hash(password, salt, function(err, hashedPasswd) {

							var fullname = firstname + ' ' + lastname;
							var currentTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

							var insertQuery = "INSERT INTO users(name, username, email, password, companyRole, levelAccess, branch, registrationDate) " +
								"VALUES ('" + fullname + "', '" + username + "', '" + email + "', '" + hashedPasswd + "', '" + companyRole + 
								"', '" + accessLevels.toString() + "', '" + branch + "', '" + currentTime + "')";

							connection.query(insertQuery, function(err, result) {
								if(err) {
									res.end('database error occured');
								}

								if(result) {
									var userId = result.insertId;
									var token = crypto.randomBytes(40).toString('hex');

									transporter.sendMail(mail_settings.mailOptionsAccActivation(email, 'Please confirm your email account', token, req), function(err, response) {
										
										if(err) {
											res.end('Error Sending the email');

										} else {

											connection.query("UPDATE users SET verifyToken ='" + token + "' WHERE id = '" + userId + "'", function(err, result) {
												
												if(err) {
													res.end('Failed to set verification token to db');
												
												} else {
													res.end('account created'); // successfully inserted
												}
											});
										}
									});
								} else {
									res.end('records insert failed'); // database error
								}
							});
						});
					});
				}
			});
		}
	});
}

/* Method that retrieves all registered accounts - and lists them on the accounts overview page */
function accountList(req, res, next) {
	connection.query("SELECT id, name, email, companyRole, levelAccess FROM users", function(err, rows) {
		res.render('account-overview', {'users': rows, loggedinUser: req.session.name, level: req.session.level});
	});
}

function listAccounts(req, res, next) {
	connection.query("SELECT id, name, email, companyRole, levelAccess FROM users", function(err, rows) {
		res.json({'users': rows, loggedinUser: req.session.name, level: req.session.level});
	});
}

/* Controller for resetting user password */
function passwordReset(req, res, next) {
	var userEmail = validator.escape(validator.trim(req.body.email));

	if(!validator.isEmail(userEmail)) {
		req.flash('tokenUpdateStatus', 'Invalid email');
		res.end('invalid_email');
	}

	connection.query("SELECT id FROM users WHERE email = '" + userEmail + "'", function(err, rows) {
		if(err){
			res.render('verification-success', {'msg': '500 - Internal Server Error'});
		}

		if(!rows.length) {
			req.flash('tokenUpdateStatus', 'No user with that email - check your email again');
			res.end('user_not_found');
		} else {
			var token = crypto.randomBytes(30).toString('hex');
			var tokenExpire = Date.now() + 3600000;
			var userId = rows[0].id;

			// token update query
			var tokenUpdateQuery = "UPDATE users SET resetPasswordToken = '" + token + "', resetPasswordExpires = '" + 
								tokenExpire + "' WHERE id = '" + userId + "' AND email = '" + userEmail + "'";

			connection.query(tokenUpdateQuery, function(err, result) {
				if(err) {
					throw err;
				}

				if(result) {
					transporter.sendMail(mail_settings.mailOptionsPasswdReset(userEmail, 'Click the link below to reset your bsoft account password', token, req), function(err, result) {
						if(err) {
							throw err;
						}

						if(result) {
							req.flash('tokenUpdateStatus', 'An email with reset link has been sent to you!');
							res.end('email_sent_success<>' + userEmail);
						}
					});
				}
			});
		}
	});
}

/* controller that sets the real password */
function validatePasswordReset(req, res) {

	if(new Buffer(req.query.QerfmRScR, 'base64').toString('ascii') === 'req_from_email') {
		connection.query("SELECT * FROM users WHERE resetPasswordToken = '" + req.params.token + "' AND resetPasswordExpires > '" + Date.now() + "'", function(err, rows) {
			if(err) {
				throw err;
			}

			if(rows.length) {
				res.render('passwordResetTpl', {'token': req.params.token});
			} else {
				req.flash('passwdResetMsg', 'Password reset token is invalid or expired');
				res.redirect('/login');
			}
		});
	} else {
		notFound404();
	}
}

function setPasswordReset(req, res) {

	connection.query("SELECT * FROM users WHERE resetPasswordToken = '" + req.params.token + "'", function(err, rows) {
		if(err) {
			throw err;
		}

		if(!rows.length) {
			req.flash('passwdResetMsg', 'Password reset token expired please try again');
			res.redirect('/login');
		}

		var user_password = req.body.password,
			user_id = rows[0].id,
			user_email = rows[0].email;


		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(user_password, salt, function(err, hashedPasswd) {
				connection.query("UPDATE users SET password = '" + hashedPasswd + "', resetPasswordToken = '', resetPasswordExpires = '' WHERE id = '" + user_id + "' AND email = '" + user_email + "'", function(err, result) {
					if(err) {
						throw err;
					}

					if(result) {
						transporter.sendMail(mail_settings.mailOptionsResetSuccess(rows[0].email, 'bsoft password reset successfull', req), function(err, result) {
							if(err) {
								throw err;
							}

							if(result) {
								req.flash('passwdResetMsg', 'Password successfully changed');
								res.redirect('/login');
							}
						});
					}
				});
			});
		});
	});
}

function getUserLevels(req, res) {
	res.json({loggedinUser: req.session.name, level: req.session.level})
}

function getUserInfo(req, res) {

	var user_id = req.params.id

	connection.query("SELECT name, username, email, companyRole, branch, levelAccess FROM users WHERE id = '" + user_id + "'", function(err, data) {
		if(err) {
			res.json({'status':'404'});
		}

		if(!data.length) {
			res.json({'status':'404'});
		}

		res.json({'status':'200', 'user': data[0]});
	});
}

module.exports = {
	verifyUser: verifyUser,
	newAccountHandler: addNewAccount,
	accountOverview: accountList,
	passwordReset: passwordReset,
	validatePasswordReset: validatePasswordReset,
	setPasswordReset: setPasswordReset,
	listAccounts: listAccounts,
	getUserLevels: getUserLevels,
	getUserInfo: getUserInfo
}