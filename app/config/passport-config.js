var LocalStrategy = require('passport-local').Strategy;
var nodemailer = require('nodemailer');
var bcrypt = require('bcrypt');
var uuid = require('node-uuid');
var mysql = require('mysql');
var crypto = require('crypto');
var moment = require('moment');
var _ = require('underscore');
var validator = require('validator');

var db_settings = require('./db_config');
var mail_settings = require('./mail_config');

/* Database setup */
var connection = mysql.createConnection(db_settings);

/* Emailing setup */
var transporter = nodemailer.createTransport(mail_settings.transport);

module.exports = function(passport) {

	passport.serializeUser(function(user, done) {
		done(null, user.id, user.levelAccess, user.name);
	});

	passport.deserializeUser(function(id, done) {

		connection.query("SELECT * FROM users WHERE id = '" + id + "'", function(err, rows) {

			if(!rows) {
				return done(null, false);
			}

			done(null, rows[0]);
		});
	});

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true

	}, function(req, email, password, done) {

		var email = validator.escape(validator.trim(email));
		var password = validator.escape(validator.trim(password));

		if(!validator.isEmail(email)) {
			return done(null, false, req.flash('signupMessage', 'Invalid email'));
		}

		connection.query("SELECT * FROM users WHERE email = '" + email + "'", function(err, rows) {
			
			if(err) {
				return done(err);
			}

			if(rows.length) {
				return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
			
			} else {

				bcrypt.genSalt(10, function(err, salt) {
					bcrypt.hash(password, salt, function(err, hashedPasswd) {

						var usersName = req.body.firstname + ' ' + req.body.secondname;

						var newUserMysql = {
							'email': email,
							'password': hashedPasswd,
							'name': usersName
						}

						// get current time to set as registration date
						var currentTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

						connection.query("INSERT INTO users (name, email, password, registrationDate) VALUES ('" + usersName + "', '" + email + "', '" + hashedPasswd + "', '" + currentTime + "')", function(err, rows) {
							
							newUserMysql.id = rows.insertId;

							var token = crypto.randomBytes(40).toString('hex');

							transporter.sendMail(mail_settings.mailOptionsAccActivation(email, 'Please confirm your email account', token, req), function(err, response) {
								
								if(err) {
									return done(null, false, {'mailErrror': 'Error Sending the email'});

								} else {
									
									connection.query("UPDATE users SET verifyToken ='" + token + "' WHERE id = '" + newUserMysql.id + "'", function(err, result) {
										
										if(err) {
											return done(null, false, {'updateError': 'Failed to set verification token to db'});
										
										} else {
											return done(null, newUserMysql);
										}
									});
								}
							});
						});
					})
				})
			}
		})
	}));


	/*  Login Functionality  */
	passport.use('local-login', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true

	}, function(req, email, password, done) {

		var email = validator.escape(validator.trim(email));
		var password = validator.escape(validator.trim(password));

		if(!validator.isEmail(email)) {
			return done(null, false, req.flash('loginMessage', 'Invalid email'));
		}

		connection.query("SELECT * FROM `users` WHERE `email` = '" + email + "'", function(err, rows) {
			if(err) {
				return done(err);
			}

			if(!rows.length) {
				return done(null, false, req.flash('loginMessage', 'No user found.'));
			}

			bcrypt.compare(password, rows[0].password, function(err, res) {

				if(err)
					return done(err);

				if(res) {
					if(rows[0].verifyToken === 'verified'){
						req.session.name = rows[0].name;
						req.session.level = rows[0].levelAccess.toString().split(',').filter(Boolean);

						return done(null, rows[0]);
					} else {
						return done(null, false, req.flash('loginMessage', 'Account not activated - an activation email was sent to you'));
					}

				} else {
					return done(null, false, req.flash('loginMessage', 'Oops! Wrong Password.'));
				}
			});
		});
	}));
}