var transportSettings = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'ochayajonathan@gmail.com',
        pass: 'ETHICALHACKING'
    }
}

function mailOptionsAccActivation(email, subject, token, req) {
	var encodeSrc = new Buffer('from_email').toString('base64');
	return {
		from: 'ochayajonathan@gmail.com',
		to: email,
		subject: subject,
		html: "Hello, Jonathan<br/> Please click on this link to verify your email.<br/><a href='" + req.protocol + "://" + req.get('host') + '/verify/' + token + "?QerfmRScR=" + encodeSrc + "'>Click here</a>"
	}
}

function mailOptionsPasswdReset(email, subject, token, req) {
	var encodeSrc = new Buffer('req_from_email').toString('base64');
	return {
		from: 'ochayajonathan@gmail.com',
		to: email,
		subject: subject,
		html: "Hello to reset your password for bsoft click the following link<br/><a href='" + req.protocol + "://" + req.get('host') + '/reset/' + token + "?QerfmRScR=" + encodeSrc +"'>Click here</a>"
	}
}

function mailOptionsResetSuccess(email, subject, req) {
	var encodeSrc = new Buffer('req_from_email').toString('base64');
	return {
		from: 'ochayajonathan@gmail.com',
		to: email,
		subject: subject,
		html: "Hello You have successfully changed your password for bsoft login here<br/><a href='" + req.protocol + "://" + req.get('host') + '/login/' + "'>Click here</a>"
	}
}

module.exports = {
	transport: transportSettings,
	mailOptionsAccActivation: mailOptionsAccActivation,
	mailOptionsPasswdReset: mailOptionsPasswdReset,
	mailOptionsResetSuccess: mailOptionsResetSuccess
}