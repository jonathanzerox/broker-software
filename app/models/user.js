var mysql = require('mysql');
var db_config = require('../config/database');

var connection = mysql.createConnection(db_config);

connection.connect(function(err) {
	if(!err) {
		console.log('Connection Established');
	} else {
		console.log('Error connecting to database');
		return false;
	}
});

module.export = connection;