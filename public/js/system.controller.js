$(function(){

	$("#email_err").hide();
	$('#not_found_msg').hide()

	$("form.passwd_rst").on('submit', function(event) {
		event.preventDefault();

		$("#email_err").hide();
		$('#not_found_msg').hide();

		var self = $(this);
		var data = self.serializeArray();

		$.ajax({
			url: self.attr('action'),
			type: 'post',
			data: data,
			success: function(returnData) {
				if(returnData.indexOf('invalid_email') != -1) {
					$("#email_err").show().css({ color: 'red'});
					$("#passwd_rst_email").css({ border: '1px solid red' });

				} else if(returnData.indexOf('user_not_found') != -1) {
					$("#email_err").hide();
					$('#not_found_msg').show().css({'color': 'red'});

				} else if(returnData.indexOf('email_sent_success') != -1) {
					var user_email = returnData.split('<>')[1];
					$(".modal-body").html("An email with further instructions has been sent to " + user_email).css({'font-size': '15px'});
				
				} else {
					$(".modal-body").html("500 Error - Server encountered an error").css({'font-size': '15px'});
				}
			}
		});
	});
});