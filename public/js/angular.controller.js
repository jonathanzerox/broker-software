var myApp = angular.module('bsoft', ['ngRoute', 'ui.bootstrap']);

myApp.config(function($routeProvider, $locationProvider) {

	$locationProvider.html5Mode(true);

	$routeProvider.
		when('/', {
			templateUrl: 'template/dashboard_partial',
			controller: 'dashboard_ctrl'
		})

		.when('/accountoverview', {
			templateUrl: 'template/accountlist_partial',
			controller: 'accountoverview_ctrl'
		})

		.when('/addaccount', {
			templateUrl: 'template/addaccount_partial',
			controller: 'addaccount_ctrl'
		})
});

myApp.controller('nav_controller', function($scope, $http) {
	$http.get('/getlevels')
	.success(function(data) {
		$scope.loggedinUser = data.loggedinUser || 'Anonymous';
	});
});

myApp.controller('dashboard_ctrl', function($scope, $http) {
	$http.get('/getlevels')
	.success(function(data) {
		$scope.level = ['Admin', 'Reports'];
	});
});

myApp.factory('userData', function () {
    return {
    	setUserData: setUserData,
    	getUserData: getUserData,
    	shared_data: {}
    };

    function setUserData(data) {
    	this.shared_data = data;
    }

    function getUserData() {
    	return this.shared_data;
    }
});

myApp.controller('accountoverview_ctrl', function($scope, $uibModal, $http, userData) {
	$http.get('/getaccounts')
	.success(function(data) {
		$scope.users = data.users;
	});

	$scope.getuser = function(user) {
		var user_id = user.currentTarget.getAttribute('data-userid'); 

		$http.get('/user/' + user_id)
		.success(function(data) {
			if(data.status == '200') {
				userData.setUserData(data.user);
				$scope.open();
			}
		});
	}

	$scope.open = function (size) {
	    var modalInstance = $uibModal.open({
	      	animation: true,
	      	templateUrl: '/template/selectedUser',
	      	controller: 'modal_ctrl',
	      	size: size
	    });
  	};
});

myApp.controller('modal_ctrl', function($scope, $uibModalInstance, userData) {

	$scope.userDetails = userData.getUserData();

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
});

myApp.controller('addaccount_ctrl', function($scope, $http) {
	$http.get('/getaccounts')
	.success(function(data) {
		$scope.users = data.users;
	});
});