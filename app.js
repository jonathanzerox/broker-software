var express = require('express');
var passport = require('passport');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var cors = require('cors');

var path = require('path');
var logger = require('morgan');

var LocalStrategy = require('passport-local').Strategy;

/*  invoke express app */
var app = express();

require('./app/config/passport-config')(passport);

/*  view engine setup */
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'jade');

/* setup middleware for serving favicon, logging, and static file serving */
app.use(favicon(path.join(__dirname, 'public', 'img/favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

/*  settup sessions - required by password library */
app.use(session({
  secret: 'brokersoftsecret', 
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

/*  require routes file */
require('./app/routes/routes')(app, passport);


/*  error handling middlewares */
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.listen(port, ipaddress);